
import {Router,Route, Switch } from 'react-router-dom'
import React from 'react';
import { createBrowserHistory as createHistory } from "history";

import Login from '../Components/Login';
import Logout from '../Components/Logout'
import Albums from '../Components/Albums/Albums';
import Artists from '../Components/Artists/Artists';
import Header from '../Components/Header';

const AppRouter = (props) => {
    let history = createHistory(props);
      console.log(history)
    return (
    <Router history={history} >
        <div>
        <Header />
            <Switch>
                <Route  path="/" component={Login} history={history}exact={true}/> 
                <Route  path={"/artists/"} component={Artists} history={history}exact={true}/> 
                <Route  path={"/albums/"} component={Albums} exact={true}/> 
                <Route  path={"/logout/"} component={Logout} exact={true}/> 
            </Switch>
        </div>
    </Router>

)} ;

export default AppRouter ;