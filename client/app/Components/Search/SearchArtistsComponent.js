import React, { Component } from 'react';
// import '../css/artists.css';

import SpotifyWebApi from 'spotify-web-api-js';


class SearchArtistComponent extends Component {
  constructor(){
    super();
    
    this.state = {
      searchValue:''
    }
  }
  
  handleSearchChange = (e) =>{
    console.log('changing : ',e.target.value);
    let value = e.target.value;
    this.setState(()=>({searchValue:value}));

    setTimeout(()=>{this.props.getArtists(this.state.searchValue)},300);
  }
  
  render() {
    return (
      <div className="search-bar">
        <input className="search-input" value={this.state.searchValue} onChange={this.handleSearchChange}/>
      </div>
    );
  }
}

export default SearchArtistComponent;
