import React, { Component } from 'react';
import '../css/artists.css';
import {logout} from '../Api/localstorage';
import {Link , NavLink} from 'react-router-dom';
import {token} from '../Api/Spotify';
class Header extends Component {
  constructor(props){
    super(props);
  }
  
  render() {
    return (
        <div className="header-bar">
          <div className="logo-container">
            <img src="http://localhost:3000/assets/logo.png" className="logo" alt=""/>
            <h2>LOGO</h2>
          </div>
          <div className="header-box-container">
            <NavLink    className="header-label" to="/artists/" exact={true}> Artists </NavLink>
          </div>
          <div className="header-box-container">
            <NavLink    className="header-label" to="/albums/" exact={true}> Albums </NavLink>
          </div>
          <div className="header-box-container">
            { token !== undefined && token !== "empty" ?
            <NavLink    className="header-label" to="/logout" exact={true}> sign out </NavLink> :
            <NavLink    className="header-label" to="/" exact={true} onClick={()=>{logout()}}> sign in </NavLink> }
          </div>
        </div>
    );
  }
}

export default Header;
