import React, { Component } from 'react';
import '../css/artists.css';
import SpotifyWebApi from 'spotify-web-api-js';
import {token,refresh} from '../Api/Spotify';
import {save} from '../Api/localstorage';

class Login extends Component {
  constructor(){
    super();
    this.state = {
      loggedIn: token!==undefined && token !== "empty" ? true : false,
    }
  }
  
  componentWillMount = ()=>{
    console.log('will mount',this.state.loggedIn);
    console.log('the token : ', token);

    if(token !== "empty" || token !== undefined){
      save(token,refresh);
    }
    
    if(this.state.loggedIn){
      this.props.history.push('/artists');
    }
  }

  
  render() {
    // console.log('props : ', this.props , this.state)
    return (
      <div className="App">
        
      {!this.state.loggedIn 
      &&
      <div className="sp-button-container">
        <div className="sp-button">
          <div>
            <img src="http://localhost:3000/assets/Spotify-icon.png"  width="50px" height="50px"/>
          </div>
          <span className="sp-text">
            <a  href='http://localhost:8888/login' > Login to Spotify </a>
          </span>
        </div>
      </div>
      } 
      </div>
    );
  }
}

export default Login;
