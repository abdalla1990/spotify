import React, { Component } from 'react';
// import '../../css/App.css';
import {token,refresh} from '../../Api/Spotify';
import Modal from 'react-modal'
class Track extends Component {

    constructor(props){
        super(props) 
        this.state={
            track : this.props.track,
            showModal:false
        }
        
    }
   
    handleCloseTracks = ()=>{this.setState(()=>({showModal:false}))}
    handleTrackClick = ()=>{
      console.log(' track clicked is ', this.state.track.id);
      this.setState(()=>({showModal:true}))
  }
  render(){
      let {track,showModal} = this.state;
        return (
            <div>
                <div className="track" onClick={this.handleTrackClick}>
                    {track.name}
                </div>


                <Modal 
                    isOpen={showModal}
                    contentLabel="Modal"
                    onRequestClose={this.handleCloseTracks}
                    className="modal"
                >
                    <button onClick={this.handleCloseTracks} id = "x">
                    X
                    </button>
                    <div className="track_info">
                        <h1>{track.name}</h1>
                        <h2>{track.artists[0].name}</h2>
                        <p>{track.duration_ms/60000} Minutes</p>
                        <iframe src={`https://open.spotify.com/embed?uri=${track.uri}`} width="100%"frameBorder="0" allowTransparency="true" allow="encrypted-media"></iframe>
                    </div>
                </Modal>
            </div>
            
        );
}
    
  
}

export default Track;
