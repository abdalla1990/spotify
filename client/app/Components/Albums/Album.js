import React, { Component } from 'react';
// import '../../css/App.css';
import {getAlbum,searchTracks,token,refresh} from '../../Api/Spotify';
import ReactStars from 'react-stars'; 
import Track from '../Tracks/Track';
class Album extends Component {

    constructor(props){
        super(props) 
        this.state={
            tracks:undefined,
            album :undefined
        }
        this.albumid= this.props.album;
    }
   
    componentWillMount = ()=>{
        getAlbum(this.albumid).then((album)=>{
            this.setState(()=>({album}));
            console.log('ALBUM : ', album);
        }).catch((error)=>{
            console.log('ERROR : ', error);
        })
    }
    handleAlbumClick = ()=>{
      searchTracks(this.albumid).then((tracks)=>{
        console.log('tracks : ', tracks);
        this.setState(()=>({tracks:tracks.items}));
      }).catch((error)=>{
        this.setState(()=>({error}));
      })

    }
    handleCloseTracks = ()=>{
        this.setState(()=>({tracks:undefined}));
    }

  render(){
      let {album,tracks} = this.state;
      console.log('ALBUM : ', album , "TRACKS : ", tracks);
        return (
            <div className="album-container">
            {tracks !== undefined && 
                <div className="tracks-container">
                    <button onClick={this.handleCloseTracks} id = "x">
                        X
                    </button>
                    {tracks.map((track,i)=>(<Track className="track" key={i} track={track}/>))}
                </div>
                }
                {album !== undefined && 
                <img className="album-image" src={album.images[0].url} />}
                {album !== undefined && 
                <p className="album-date">{album.release_date}</p>}
                {album !== undefined && 
                <div className="artist-rating">
                <ReactStars
                    count={5}
                    color1={"black"}
                    value={(album.popularity/2)/10}
                    edit={false}
                    size={24}
                    color2={'#D2691E'} />
                </div>}
                {album !== undefined && 
                    <div className="tracks-list" onClick={this.handleAlbumClick}>
                    <p>Tracks</p>
                    </div>
                }

            </div>
        );
}
    
  
}

export default Album;
