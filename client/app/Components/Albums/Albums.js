import React, { Component } from 'react';
// import '../../css/App.css';
import Album from './Album';
import {searchAlbums,getAlbum,searchTracks,token,refresh} from '../../Api/Spotify';
import SearchAlbumComponent from '../Search/SearchAlbumComponent';
class Albums extends Component {
  constructor(props){
    super(props);
    this.state = {
      albums : []
    }
  }
  
  componentWillMount = ()=>{
    if(token === "empty" || token === undefined){
      this.props.history.push('/');
      return
    }
    
    if(this.props.location.state){
      this.sort(this.props.location.state.artistAlbums.items);
    }
    
  }

  sort = (albums)=>{
    
    console.log('inside sort , albums : ',albums);
    albums.sort((a, b) =>{
      // console.log(a.release_date.substring(0,4) , b.release_date.substring(0,4))
      return a.release_date.substring(0,4) - b.release_date.substring(0,4)
    });
    this.setState(()=>({albums}));
    
    // console.log('albartistAlbumsums : ',this.state.albums);
  }


  getAlbums = (searchQuery)=>{
    console.log(searchQuery);
    searchAlbums(searchQuery).then((albums)=>{
      console.log('albums : ',albums);
     
        this.sort(albums.albums.items);
      
      
    }).catch((error)=>{
      this.setState(()=>({error , albums:undefined}));
    })
  }
  
  
  
  render() {
        
        let {albums} = this.state;
        // console.log('albartistAlbumsums : ',albums);
        return (
          <div>
            <div className="search-box-container">
            {this.state.error && <p>No Data</p>}
            <div className="search-label-container">
              <h3 className="search-label">Search for albums.</h3>
            </div>
              <SearchAlbumComponent getAlbums={this.getAlbums}/>
            </div>
            <div className="albums-container">
                {albums !== undefined && albums.map((album,index)=>(<Album key={index} album={album.id}/>))}
            </div>
          
          </div>
         
        );
  }
}

export default Albums;
