import React, { Component } from 'react';
// import '../css/artists.css';
import Artist from './Artist';

class ArtistList extends Component {
  constructor(){
    super();
  }
  
  
  
  render() {
      console.log('artists : ',this.props);

          return(
            <div className="artists">
            
                {   this.props.artists !== undefined && 
                    this.props.artists.map((artist,index)=>{
                        return (<Artist history={this.props.history} key={index} artist={artist}/>)
                    })  
                } 
            
            </div>
        );
          
      }
    
  
}

export default ArtistList;
