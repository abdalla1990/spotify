import React, { Component } from 'react';
import SearchArtistsComponent from '../Search/SearchArtistsComponent';
import ArtistsList from './ArtistsList';
// import '../../css/artists.css';
import {searchArtists, token,refresh} from '../../Api/Spotify';
class Artists extends Component {
  constructor(props){
    super(props);
    this.state = {
      artists:[],
      error:undefined
    }
  }
  componentWillMount = ()=>{
    if(token === "empty" || token === undefined){
      this.props.history.push('/');
    }
  }
  getArtists = (searchQuery) => {
    
    searchArtists(searchQuery).then((artists)=>{
      this.setState(()=>({artists,error:undefined}));
    }).catch((error)=>{
      this.setState(()=>({error}));
    })
  }
  render() {
    console.log('props : ', this.props)
    return (
      <div>
        <div className="search-box-container" >
          {this.state.error && <p className="search-label">No Results !</p>}
          <div className="search-label-container">
            <h3 className="search-label">Search for artists.</h3>
          </div>
          <SearchArtistsComponent getArtists={this.getArtists}/>
        </div>
        <div className="">
          {!this.state.error &&<ArtistsList history={this.props.history} artists={this.state.artists}/>}
        </div>
        
      </div>
    );
  }
}

export default Artists;
