import React, { Component } from 'react';
// import '../css/artists.css';
import Albums from '../Albums/Albums'
import ReactStars from 'react-stars'; 
import {searchAlbums,token,refresh} from '../../Api/Spotify';

class Artist extends Component {
  constructor(props){
    super(props);
    this.state = {
        artistClicked : undefined,
        albums : undefined
    }
  }
  
  handleArtistClick=()=>{
    
      
      searchAlbums(this.props.artist.name).then((albums)=>{
        
        // console.log('history : ', this.props);
        let artistAlbums= albums.albums
        this.props.history.push({
            pathname:'/albums',
            state : {artistAlbums},
            hash:`access_token=${token}&refresh_token=${refresh}`
        });
      }).catch((error)=>{
        this.setState(()=>({error}));
      })
  }
  
  render() {
        // console.log('artists : ',this.props.artist);
        let {artist} = this.props;
        let {state}  = this.state;
        return (
            <div>
                <div className="artist" onClick={this.handleArtistClick}>
                    {this.state.error}
                    <img className="artist-image" src={artist.images.length !== 0?artist.images[0].url : ""} height="500px"width="100%"/>
                    <h2 className="artist-label">{artist.name}</h2> 
                    <div className="artist-rating">
                        <ReactStars
                            count={5}
                            color1={"black"}
                            value={(artist.popularity/2)/10}
                            edit={false}
                            size={24}
                            color2={'#D2691E'} />
                    </div>
                    <div className="artist-gen-container">
                        {artist.genres.map((genre,index)=>{
                            if(index < 4){
                            return( <div className="artist-gen"><p key={index}>{genre}</p></div> )
                            }
                            
                        })}
                    </div>
                </div>
            </div>
        );
  }
}

export default Artist;
