import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import registerServiceWorker from './registerServiceWorker';
import AppRouter from './Router/AppRouter';

let Jsx =(<div className="app-container"><AppRouter /></div>)
ReactDOM.render(Jsx, document.getElementById('root'));
registerServiceWorker();
