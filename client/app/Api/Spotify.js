import SpotifyWebApi from 'spotify-web-api-js';
import {load} from './localstorage';
const spotifyApi = new SpotifyWebApi();

let getHashParams=()=>{
    var hashParams = {};
    var e, r = /([^&;=]+)=?([^&;]*)/g,
        q = window.location.hash.substring(1);
    e = r.exec(q)
    while (e) {
        hashParams[e[1]] = decodeURIComponent(e[2]);
        e = r.exec(q);
    }
    console.log('in hashparams : ', hashParams);
    return hashParams;
}



let getAuth=()=>{
    
    let data = load();
    console.log('data',data);
    if(data === undefined || data.token === "empty"){
        const params = getHashParams();
        if(params.access_token !== undefined){
            return{
                token : params.access_token,
                refresh : params.refresh_token
            }
        }else{
            return {
                token:"empty",
                refresh:"empty"
            }
        }
        
    }else{
        return data;
    }
}

const params = getAuth();
export const token = params.token;
export const refresh = params.refresh;



   


spotifyApi.setAccessToken(token);


export let searchAlbums = (artistName)=>{
    let promise = new Promise((resolve,reject)=>{
        spotifyApi.searchAlbums(artistName,{limit:40}).then((albums)=>{
            return resolve(albums);
        }).catch((err)=>{
            console.log('error ',err.toString())
             return reject(err.toString())
        });
    });
    return promise;
}

export let searchArtists = (query)=>{
    let promise = new Promise((resolve,reject)=>{
        spotifyApi.searchArtists(query,{limit:40}).then((response) => {
            return resolve(response.artists.items);
         }).catch((err)=>{
             console.log('error ',err.toString())
             return reject(err.toString())
         });
    
    });

    return promise;
    
}

export let searchTracks = (albumId)=>{
    let promise = new Promise((resolve,reject)=>{
        spotifyApi.getAlbumTracks(albumId,{limit:40}).then((response) => {
            return resolve(response);
         }).catch((err)=>{
             console.log('error ',err.toString())
             return reject(err.toString())
         });
    
    });

    return promise;
}

export let getAlbum = (albumId)=>{
    let promise = new Promise((resolve,reject)=>{
        spotifyApi.getAlbum(albumId).then((response) => {
            return resolve(response);
         }).catch((err)=>{
             console.log('error ',err.toString())
             return reject(err.toString())
         });
    
    });

    return promise;
}

export let searchTracksByQuery = (query)=>{
    let promise = new Promise((resolve,reject)=>{
        spotifyApi.searchTracks(query,{limit:40}).then((response) => {
            return resolve(response);
         }).catch((err)=>{
             console.log('error ',err.toString())
             return reject(err.toString())
         });
    
    });

    return promise;
}

export let getTrack = (trackId)=>{
    let promise = new Promise((resolve,reject)=>{
        spotifyApi.getTrack(trackId,{limit:40}).then((response) => {
            return resolve(response);
         }).catch((err)=>{
             console.log('error ',err.toString())
             return reject(err.toString())
         });
    
    });

    return promise;
}


