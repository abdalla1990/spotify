export const load = ()=>{
    try { 
    //    localStorage.clear();
        const serializedToken = localStorage.getItem('token');
        const serializedRefresh = localStorage.getItem('refresh');
        console.log('in localstorage the state after load is : ', serializedToken);
        if(serializedToken === null || serializedRefresh === null){
            return undefined ;
        }else{
          return  {token : JSON.parse(serializedToken) , refresh : JSON.parse(serializedRefresh)}
        }
    }catch(exeption){
        return undefined;
    }
}


export const save = (token , refresh)=>{
    console.log('token', token , refresh);
    try {
    
        console.log('in localstorage : the state ', token,refresh);
        localStorage.setItem('token',JSON.stringify(token));
        localStorage.setItem('refresh',JSON.stringify(refresh));

       
    }catch(err){
        throw err;
    }


}

export let logout = () => {
    try {
           localStorage.clear();
           let data =load();
           console.log(data)
    }catch(err){

    }
}